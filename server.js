var express = require('express');  // require express
var app = express();  // create a request handler function
var server = require('http').createServer(app);  // use our app to create a server
var io = require('socket.io')(server); // pass in our http app server to get a Socket.io server
var path = require('path');
var ejs =  require('ejs');
var bodyParser =  require('body-parser');
var logger = require("morgan");

app.use(express.static(__dirname+'/views'))
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs");

//app.get('/', function(req, res){
  //app.use(express.static(path.join(__dirname)));
  //res.sendFile(path.join(__dirname, '../A03/views', 'index.html'));
//}); // specify our view engine

// manage our entries
var entries = [];
app.locals.entries = entries;

// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

// on a GET request to default page, do this.... 
app.get('/', function(req, res){
      res.render('index.html');
});
// app.get('/', function(req,res){
// 	res.render('aboutme');
// })

// app.post("/new-entry",function(req,res){
//   var api_key = 'key-36784054626b6c63bcfcb37f398d8813';
// var domain = 'sandbox5d3b18c3ac6348b3a93227b3ff72f36b.mailgun.org';
// var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
// var data = {
//   from: 'Supriya <postmaster@sandbox5d3b18c3ac6348b3a93227b3ff72f36b.mailgun.org>',
//   to: 'serobnic@mail.ru',
//   subject: req.body.NAME,
//   //+"<"+req.body.email+">"+" Sent you a message", //Subject Line
//     //html:req.body. //Subject Body
// };
 
// mailgun.messages().send(data, function (error, body) {
//   console.log(body);
//   if(!error)
//   res.send("Mail Sent");
//   else
//   res.send("Mail not sent");
// });
// });

app.get('/guestbook', function(req,res){
  res.render('gbindex');

});

app.post('/guestbook', function(req,res){
  res.render('gbindex');

});

// http POST (INSERT)
app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  console.log(entries.length);
  response.redirect("/guestbook");
  res.render('gbindex')
  //response.redirect("/");
});

app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

app.use(function (request, response) {
  response.status(404).render("404");
});

// Listen for an app request on port 1234
server.listen(1234, function(){
  console.log('listening on http://127.0.0.1:1234/');
});